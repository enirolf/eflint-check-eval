# eFLINT-Check Evaluation Specifications

This repository contains the set of eFLINT specifications and eFLINT-LTL properties that were used in the validation and evaluation of the eFLINT-Check model checker.

## Set-up

Requires Python >= 3.9. We recommend using a virtual environment to install the dependencies:

```sh
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

To exit the virtual environment, run
```sh
deactivate
```

## Validation
Validate `eflint-check` with a set of specifications and properties, and check if they match the expected results.

Usage:
```sh
python validate.py
```

A utility script to convert the validation files to LaTeX is also available:
```sh
python to_tex.py
```
