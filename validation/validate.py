#!/usr/bin/env python3

from os import DirEntry, scandir
from os.path import basename
from subprocess import run
from re import findall


def get_specs() -> list[DirEntry]:
    with scandir(".") as it:
        return [e for e in it if e.name.endswith(".eflint")]


def get_props(spec_path: DirEntry) -> list[tuple[str, str]]:
    with open(spec_path) as f:
        props: list[tuple[str, str]] = []

        for line in f:
            props += tuple(findall(r"Property (.*) Where ([^\.\n]*)", line))

        return props


def check(spec_path: DirEntry, prop_name: str, prop: str) -> None:
    res = run(["eflint-check", "-p", prop_name, spec_path], capture_output=True)

    try:
        if "good" in prop_name:
            assert (
                b"No counterexamples found!" in res.stdout
            ), f"Unexpected counterexample found for {prop_name}"
        else:
            assert (
                b"Counterexample found!" in res.stdout
            ), f"No expected counterexample found for {prop_name}"
    except AssertionError as e:
        print(" Assertion error ".center(80, "!"))
        print(f"{e}:\n")
        print(prop.center(80))
        print("\nRun this example with the following command:\n")
        print(f"eflint-check -p {prop_name} {spec_path.name}".center(80))
        print("".center(80, "!"))
        exit(1)
    else:
        print(f"{prop_name}: ✔︎")


def main() -> None:
    specs = get_specs()

    for spec in specs:
        props = get_props(spec)

        print(f" {spec.name} ".center(80, "="))

        for prop in props:
            check(spec, *prop)


if __name__ == "__main__":
    main()
