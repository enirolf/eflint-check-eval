#!/usr/bin/env python3

# Convert file contents to LaTeX listings, so they can be added as appendices.

from os import DirEntry, scandir
from os.path import basename, splitext


def get_specs() -> [DirEntry]:
    with scandir(".") as it:
        return [e for e in it if e.name.endswith(".eflint")]


def to_listing(path: DirEntry) -> str:
    with open(path) as f:
        caption = path.name.replace("_", "\\_")
        return (
            f"\\begin{{lstlisting}}[language=eflint]\n"
            f"{f.read()}"
            f"\\end{{lstlisting}}"
        )


def main() -> None:
    specs = get_specs()

    with open("output.tex", "w+") as f:
        f.write("\\chapter{Validation specifications and properties}\n")
        for spec in specs:
            title = splitext(spec.name)[0].replace("_", " ").capitalize()
            f.write(f"\\section{{{title}}}\n\n")
            f.write(to_listing(spec))
            f.write("\n\n")


if __name__ == "__main__":
    main()
