#!/user/bin/env python3

from os import makedirs

import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.rcsetup import cycler

# matplotlib.use("pgf")
CMAP = ["royalblue", "crimson", "seagreen", "darkorange"]
matplotlib.rcParams.update(
    {
        "pgf.texsystem": "xelatex",
        "font.family": "serif",
        "text.usetex": True,
        "pgf.rcfonts": False,
        "axes.prop_cycle": cycler("color", CMAP),
    }
)


def single_act_spec_literal_size() -> None:
    directory = "figures/single_act_spec_literal_size"
    makedirs(directory, exist_ok=True)
    data = pd.read_pickle("data/single_act_spec_literal_size.pkl")

    print(data)

    data = data / 1000000

    # fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(9, 3))

    xticks = data.T.index
    plt.set_cmap

    plt.figure("literal_compile_size", figsize=(3, 3), layout="tight")
    plt.plot(data.loc["compile_size"].T)
    plt.xlabel("Domain size for fact-type \\texttt{{atom}}")
    plt.ylabel("\\textsc{{nu}}X\\textsc{{mv}} input size (in MB)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/compile_size.pgf")
    plt.savefig(f"{directory}/compile_size.pdf")

    plt.figure("literal_compile_time", figsize=(3, 3), layout="tight")
    plt.errorbar(
        xticks, data.loc["compile_time"].mean(), yerr=data.loc["compile_time"].std()
    )
    plt.xlabel("Domain size for fact-type \\texttt{{atom}}")
    plt.ylabel("e\\textsc{{flint-check}} translation time (in ms)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/compile_time.pgf")
    plt.savefig(f"{directory}/compile_time.pdf")

    plt.figure("literal_check_time", figsize=(3, 3), layout="tight")
    plt.errorbar(
        xticks, data.loc["check_time"].mean(), yerr=data.loc["check_time"].std()
    )
    plt.xlabel("Domain size for fact-type \\texttt{{atom}}")
    plt.ylabel("\\textsc{{nu}}X\\textsc{{mv}} model check time (in ms)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/check_time.pgf")
    plt.savefig(f"{directory}/check_time.pdf")

    # # ax1.figure("literal_compile_size")
    # ax1.plot(data.loc["compile_size"].T)
    # ax1.set_xlabel("Domain size for fact-type \\texttt{{atom}}")
    # ax1.set_ylabel("\\textsc{{nu}}X\\textsc{{mv}} input size (in MB)")
    # ax1.set_xticks(xticks)
    # # plt.savefig(f"{directory}/compile_size.pgf")

    # # plt.figure("literal_compile_time")
    # ax2.errorbar(
    #     xticks, data.loc["compile_time"].mean(), yerr=data.loc["compile_time"].std()
    # )
    # ax2.set_xlabel("Domain size for fact-type \\texttt{{atom}}")
    # ax2.set_ylabel("e\\textsc{{flint-check}} translation time (in ms)")
    # ax2.set_xticks(xticks)
    # # ax2.savefig(f"{directory}/compile_time.pgf")

    # # ax2.figure("literal_check_time")
    # ax3.errorbar(
    #     xticks, data.loc["check_time"].mean(), yerr=data.loc["check_time"].std()
    # )
    # ax3.set_xlabel("Domain size for fact-type \\texttt{{atom}}")
    # ax3.set_ylabel("\\textsc{{nu}}X\\textsc{{mv}} model check time (in ms)")
    # ax3.set_xticks(xticks)
    # # ax3.savefig(f"{directory}/check_time.pgf")

    # fig.tight_layout()
    # fig.savefig(f"{directory}/subplot.pgf")
    # fig.savefig(f"{directory}/subplot.pdf")


def single_act_spec_field_size() -> None:
    directory = "figures/single_act_spec_field_size"
    makedirs(directory, exist_ok=True)
    data = pd.read_pickle("data/single_act_spec_field_size.pkl")

    print(data)

    data = data / 1000000

    xticks = data.T.index
    plt.set_cmap

    plt.figure("field_compile_size", figsize=(3, 3), layout="tight")
    plt.plot(data.loc["compile_size"].T)
    plt.xlabel("Field size for fact-type \\texttt{{record}}")
    plt.ylabel("\\textsc{{nu}}X\\textsc{{mv}} input size (in MB)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/compile_size.pgf")
    plt.savefig(f"{directory}/compile_size.pdf")

    plt.figure("field_compile_time", figsize=(3, 3), layout="tight")
    plt.errorbar(
        xticks, data.loc["compile_time"].mean(), yerr=data.loc["compile_time"].std()
    )
    plt.xlabel("Field size for fact-type \\texttt{{record}}")
    plt.ylabel("e\\textsc{{flint-check}} translation time (in ms)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/compile_time.pgf")
    plt.savefig(f"{directory}/compile_time.pdf")

    plt.figure("field_check_time", figsize=(3, 3), layout="tight")
    plt.errorbar(
        xticks, data.loc["check_time"].mean(), yerr=data.loc["check_time"].std()
    )
    plt.xlabel("Field size for fact-type \\texttt{{record}}")
    plt.ylabel("\\textsc{{nu}}X\\textsc{{mv}} model check time (in ms)")
    plt.xticks(xticks)
    plt.savefig(f"{directory}/check_time.pgf")
    plt.savefig(f"{directory}/check_time.pdf")

    # fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(9, 3))
    # # ax1.figure("literal_compile_size")
    # ax1.plot(data.loc["compile_size"].T)
    # ax1.set_xlabel("Field size for fact-type \\texttt{{record}}")
    # ax1.set_ylabel("\\textsc{{nu}}X\\textsc{{mv}} input size (in MB)")
    # ax1.set_xticks(xticks)
    # # plt.savefig(f"{directory}/compile_size.pgf")

    # # plt.figure("literal_compile_time")
    # ax2.errorbar(
    #     xticks, data.loc["compile_time"].mean(), yerr=data.loc["compile_time"].std()
    # )
    # ax2.set_xlabel("Field size for fact-type \\texttt{{record}}")
    # ax2.set_ylabel("e\\textsc{{flint-check}} translation time (in ms)")
    # ax2.set_xticks(xticks)
    # # ax2.savefig(f"{directory}/compile_time.pgf")

    # # ax2.figure("literal_check_time")
    # ax3.errorbar(
    #     xticks, data.loc["check_time"].mean(), yerr=data.loc["check_time"].std()
    # )
    # ax3.set_xlabel("Field size for fact-type \\texttt{{record}}")
    # ax3.set_ylabel("\\textsc{{nu}}X\\textsc{{mv}} model check time (in ms)")
    # ax3.set_xticks(xticks)
    # # ax3.savefig(f"{directory}/check_time.pgf")

    # fig.tight_layout()
    # fig.savefig(f"{directory}/subplot.pgf")
    # fig.savefig(f"{directory}/subplot.pdf")


def main() -> None:
    makedirs("figures", exist_ok=True)
    single_act_spec_literal_size()
    single_act_spec_field_size()
    plt.show()


if __name__ == "__main__":
    main()
