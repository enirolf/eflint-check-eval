#!/user/bin/env python3

from os import DirEntry, makedirs
from os.path import basename, splitext

from time import process_time_ns

from tempfile import NamedTemporaryFile
from subprocess import run

from collections import namedtuple
from collections.abc import Callable
from typing import NamedTuple

from random import choices
from string import ascii_uppercase, ascii_lowercase

from csv import DictWriter
import pandas as pd

from datetime import datetime

import chevron  # type: ignore[import]

StringLiteral = namedtuple("StringLiteral", ["ident", "last"])
FieldParam = namedtuple("FieldParam", ["ident", "last"])


def generate_literals(n: int) -> list[StringLiteral]:
    literals = []

    for i in range(n):
        ident = "".join(choices(ascii_uppercase, k=3))
        last = i == n - 1
        literals.append(StringLiteral(ident, last))

    return literals


def generate_field_params(n: int) -> list[FieldParam]:
    field_params = []

    for i in range(n - 2):
        ident = "".join(choices(ascii_lowercase, k=3))
        last = i == n - 3
        field_params.append(FieldParam(ident, last))

    return field_params


def eval(
    spec_func: Callable[[int], str], upper_bound: int, repetitions: int = 1
) -> None:
    data: list[dict[str, int]] = []

    compile_size_table = pd.DataFrame()
    compile_time_table = pd.DataFrame()
    check_time_table = pd.DataFrame()

    for i in range(1, upper_bound + 1):
        print(f"Running for bound of {i}...")
        with NamedTemporaryFile(suffix=".eflint") as tf:
            spec = tf.write(bytes(spec_func(i), "utf-8"))
            tf.seek(0)

            compile_times = []
            for _ in range(repetitions):
                ct0 = process_time_ns()
                cres = run(["eflint-check", "-Px", tf.name], capture_output=True)
                ct1 = process_time_ns()
                ctd = ct1 - ct0
                compile_times.append(ctd)

            compile_size_table.insert(i - 1, i, [len(cres.stdout)])
            compile_time_table.insert(i - 1, i, compile_times)

            smv_name = splitext(basename(tf.name))[0] + ".smv"
            source_name = splitext(basename(tf.name))[0] + ".scr"

            check_times = []
            for _ in range(repetitions):
                nt0 = process_time_ns()
                run(
                    [
                        "nuxmv",
                        "-source",
                        f".eflint-check/{source_name}",
                        f".eflint-check/{smv_name}",
                    ],
                    capture_output=True,
                )
                nt1 = process_time_ns()
                ntd = nt1 - nt0
                check_times.append(ntd)

            check_time_table.insert(i - 1, i, check_times)

    print("All done!")

    table = pd.concat(
        [compile_size_table, compile_time_table, check_time_table],
        keys=["compile_size", "compile_time", "check_time"],
    )

    print(table)
    ident = datetime.now().strftime("%s")
    table.to_pickle(f"data/{spec_func.__name__}_{ident}.pkl")
    # print(compile_size_table)
    # print(compile_time_table)
    # print(check_time_table)

    # with open(f"data/{spec_func.__name__}.csv", "w", newline="") as csvfile:
    #     fieldnames = ["free_var", "smv_size", "compile_time", "solve_time"]
    #     writer = DictWriter(csvfile, fieldnames=fieldnames)

    #     writer.writeheader()
    #     writer.writerows(data)


def single_act_spec_literal_size(num_literals: int) -> str:
    literals = generate_literals(num_literals)

    data = {"literals": map(lambda l: l._asdict(), literals)}

    with open("templates/single_act_literal_size.mustache") as f:
        spec = chevron.render(f, data)
        return spec


def single_act_spec_field_size(num_fields: int) -> str:
    fields = generate_field_params(num_fields)

    data = {
        "fields1": map(lambda l: l._asdict(), fields),
        "fields2": map(lambda l: l._asdict(), fields),
        "fields3": map(lambda l: l._asdict(), fields),
        "fields4": map(lambda l: l._asdict(), fields),
        "fields5": map(lambda l: l._asdict(), fields),
        "fields6": map(lambda l: l._asdict(), fields),
        "fields7": map(lambda l: l._asdict(), fields),
    }

    with open("templates/single_act_field_size.mustache") as f:
        spec = chevron.render(f, data)
        return spec


def create_act_loop_spec(num_literals: int) -> str:
    literals = generate_literals(num_literals)

    data = {"literals": map(lambda l: l._asdict(), literals)}

    with open("templates/act_loop.mustache") as f:
        spec = chevron.render(f, data)
        return spec


def main():
    makedirs("data", exist_ok=True)
    # spec = single_act_spec_field_size(3)
    # print(spec)
    # eval(single_act_spec_literal_size, 10, 5)
    eval(single_act_spec_field_size, 7, 5)
    # eval_smv_compilation(single_act_spec_field_size, 10)


if __name__ == "__main__":
    main()
